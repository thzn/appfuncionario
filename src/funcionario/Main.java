package funcionario;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private static Stage stage;
    private static Scene login;
    private static Scene cadastro;
    private static Scene menu;
    @Override
    public void start(Stage window) throws Exception {
        stage = window;
        window.setTitle("Controle de Funcionarios");
        Parent fxmlLogin = FXMLLoader.load(getClass().getResource("../views/Login.fxml"));
        login = new Scene(fxmlLogin);
        Parent fxmlMenu = FXMLLoader.load(getClass().getResource("../views/MenuPrincipal.fxml"));
        menu = new Scene(fxmlMenu);
        Parent fxmlCadastro = FXMLLoader.load(getClass().getResource("../views/Cadastro.fxml"));
        cadastro = new Scene(fxmlCadastro);
        window.setScene(login);
        window.show();      
    }
    public static void changeScreen(String scr){
        switch(scr){
            case "login":
                stage.setScene(login);
                break;
            case "menu":
                stage.setScene(menu);
                stage.setMaximized(true);
                break;
            case "cadastro":
                stage.setScene(cadastro);
                stage.setMaximized(true);
                break;
        }
    }
    public static void main(String[] args) {
        launch(args);  
    }
}